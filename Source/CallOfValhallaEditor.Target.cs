// Copyright ICFHAG Studio. All rights reserved. 2021

using UnrealBuildTool;
using System.Collections.Generic;

public class CallOfValhallaEditorTarget : TargetRules
{
	public CallOfValhallaEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "CallOfValhalla" } );
	}
}
