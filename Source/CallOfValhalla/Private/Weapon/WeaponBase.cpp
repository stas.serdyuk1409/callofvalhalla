// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Weapon/WeaponBase.h"
#include "Components/ArrowComponent.h"
#include "../Public/Weapon/Projectile/ProjectileWeaponBase.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Public/Character/BasePlayerCharacter.h"
#include "Engine/StaticMeshActor.h"
#include "DrawDebugHelpers.h"
#include "../Public/Character/COVInventoryCharacter.h"
#include "Particles/ParticleSystemComponent.h"

#include "Perception/AISense_Damage.h"

#include "Net/UnrealNetwork.h"
#include "../CallOfValhalla.h"

int32 DebugWeaponShow = 0;
FAutoConsoleVariableRef CVarWeaponShow(
	TEXT("COV.DebugWeapon"),
	DebugWeaponShow,
	TEXT("Draw Debug for Weapon"),
	ECVF_Cheat);


AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshComponent->SetGenerateOverlapEvents(false);
	SkeletalMeshComponent->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshComponent->SetupAttachment(SceneComponent);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshComponent->SetGenerateOverlapEvents(false);
	StaticMeshComponent->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshComponent->SetupAttachment(SceneComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(SceneComponent);
	bReplicates = true;
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	if (!SkeletalMeshComponent)
	{
		SkeletalMeshComponent->DestroyComponent();
	}
	if (!StaticMeshComponent)
	{
		StaticMeshComponent->DestroyComponent();
	}
	WeaponInit();
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		UseWeaponTick(DeltaTime);
		CooldownWeaponTick(DeltaTime);
		DispertionTick(DeltaTime);
		DropMeshTick(DeltaTime);
		DropOtherMeshTick(DeltaTime);
		
	}
	DestroyTraceFXTick_Multicast_Implementation(DeltaTime);

}
void AWeaponBase::DestroyTraceFXTick_Multicast_Implementation(float DeltaTime)
{
	if (DestroyTrace)
	{
		if (TimeTrace <= 0)
		{
			DestroyTrace = false;
			if (TraceEmittor)
				TraceEmittor->DestroyComponent();
			TimeTrace = 0.25f;
		}
		else
		{
			TimeTrace -= DeltaTime;
		}
	}
}
// ��� �������

void AWeaponBase::CooldownWeaponTick(float DeltaTime)
{
	if (bWeaponCooldown)
	{
		if (CooldownRate < 0.0f)
		{
			FinishCooldownToUse();
		}
		else
		{
			CooldownRate -= DeltaTime;
		}
	}
}

void AWeaponBase::DispertionTick(float DeltaTime)
{
	if (!bWeaponCooldown)
	{
		if (!bWeaponUse)
		{
			if (bShouldReduceDispersion)
			{
				fCurrentDispersion = fCurrentDispersion - fCurrentDispersionReduction;
			}
			else
			{
				fCurrentDispersion = fCurrentDispersion + fCurrentDispersionReduction;
			}
		}
		if (fCurrentDispersion < fCurrentDispersionMin)
		{
			fCurrentDispersion = fCurrentDispersionMin;
		}
		else
		{
			if (fCurrentDispersion > fCurrentDispersionMax)
			{
				fCurrentDispersion = fCurrentDispersionMax;
			}
		}

	}
	if (DebugWeaponShow)
	{
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f, MIN = %f, Current = %f"), fCurrentDispersionMax, fCurrentDispersionMin, fCurrentDispersion);
	}
}

void AWeaponBase::DropOtherMeshTick(float DeltaTime)
{
	if (DropOtherMeshFlag)
	{
		if (DropOtherMeshTimer < 0.f)
		{
			DropOtherMeshFlag = false;
			InitDropMesh_OnServer(WeaponSetting.DropOtherMeshSetting.DropOtherMesh,
				WeaponSetting.DropOtherMeshSetting.DropMeshOffset,
				WeaponSetting.DropOtherMeshSetting.DropImppulseMesh,
				WeaponSetting.DropOtherMeshSetting.fDropLifeTime,
				WeaponSetting.DropOtherMeshSetting.fImpulseRandomDespersion,
				WeaponSetting.DropOtherMeshSetting.fPowerImpulse,
				WeaponSetting.DropOtherMeshSetting.fCustomMass);
		}
		else
		{
			DropOtherMeshTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::DropMeshTick(float DeltaTime)
{
	if (DropMeshFlag)
	{
		if (DropMeshTimer < 0.f)
		{
			DropMeshFlag = false;
			InitDropMesh_OnServer(WeaponSetting.DropMeshSetting.DropMesh,
				WeaponSetting.DropMeshSetting.DropMeshOffset,
				WeaponSetting.DropMeshSetting.DropImppulseMesh,
				WeaponSetting.DropMeshSetting.fDropLifeTime,
				WeaponSetting.DropMeshSetting.fImpulseRandomDespersion,
				WeaponSetting.DropMeshSetting.fPowerImpulse,
				WeaponSetting.DropMeshSetting.fCustomMass);
		}
		else
		{
			DropMeshTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::UseWeaponTick(float DeltaTime)
{
	if (bWeaponUse && GetWeaponRound() > 0 && !bWeaponCooldown)
		{

			if (SpeedRate < 0.f)
			{
					if (!bWeaponCooldown)
					{
						UseWeapon();
					}
			}
			else
			{
				SpeedRate -= DeltaTime;
			}
		}


}

//��� �������

// ���������������� ������� 

void AWeaponBase::WeaponInit()
{
	if (SkeletalMeshComponent && !SkeletalMeshComponent->SkeletalMesh)
	{
		SkeletalMeshComponent->DestroyComponent(true);
	}
	if (StaticMeshComponent && !StaticMeshComponent->GetStaticMesh())
	{
		StaticMeshComponent->DestroyComponent(true);
	}
	
	
}

void AWeaponBase::InitCooldown()
{
	bWeaponCooldown = true;
	CooldownRate = WeaponSetting.fReUseTime;
	CooldownTime = CooldownRate;
	// ����� ������� ������ DropMesh
	if (WeaponSetting.DropMeshSetting.DropMesh)
	{
		if (WeaponSetting.DropOtherMeshSetting.fDropTime < 0.f)
		{
			InitDropMesh_OnServer(WeaponSetting.DropMeshSetting.DropMesh,
				WeaponSetting.DropMeshSetting.DropMeshOffset,
				WeaponSetting.DropMeshSetting.DropImppulseMesh,
				WeaponSetting.DropMeshSetting.fDropLifeTime,
				WeaponSetting.DropMeshSetting.fImpulseRandomDespersion,
				WeaponSetting.DropMeshSetting.fPowerImpulse,
				WeaponSetting.DropMeshSetting.fCustomMass);
		}
		else
		{
			DropMeshFlag = true;
			DropMeshTimer = WeaponSetting.DropMeshSetting.fDropTime;
		}
	}
	// ����� �������� �� ����������� ������
	OnWeapopReloadStart.Broadcast(WeaponSetting.AnimReUsingWeapon, WeaponSetting.fReUseTime);
}

void AWeaponBase::InitDropMesh_OnServer_Implementation(UStaticMesh* MeshToDrop, FTransform Offset, FVector ImpulseDrop, float LifeTime, float ImpulseRandomDespersion, float PowerImpulse, float CustomMass)
{
	if (MeshToDrop)
	{
		FTransform Transform;
		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;
		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		
	
		InitShellDrop_Multicast(MeshToDrop, Transform, ImpulseDrop, LifeTime, ImpulseRandomDespersion, PowerImpulse, CustomMass, LocalDir);
		


	}
}

// ���������������� ������� 

// ������

void AWeaponBase::UseWeapon()
{
	SpeedRate = WeaponSetting.fUsingSpeedRate;
	WeaponInfo.Round--;
	
	// ����� �������� �� ������������� ������
	//if(WeaponSetting.AnimUsingWeapon)
	OnWeapopUseStart.Broadcast(WeaponSetting.AnimUsingWeapon, WeaponSetting.fUsingSpeedRate);

	if (WeaponSetting.DropOtherMeshSetting.DropOtherMesh)
	{
		if (WeaponSetting.DropOtherMeshSetting.fDropTime < 0.f)
		{
			InitDropMesh_OnServer(WeaponSetting.DropOtherMeshSetting.DropOtherMesh,
				WeaponSetting.DropOtherMeshSetting.DropMeshOffset,
				WeaponSetting.DropOtherMeshSetting.DropImppulseMesh,
				WeaponSetting.DropOtherMeshSetting.fDropLifeTime,
				WeaponSetting.DropOtherMeshSetting.fImpulseRandomDespersion,
				WeaponSetting.DropOtherMeshSetting.fPowerImpulse,
				WeaponSetting.DropOtherMeshSetting.fCustomMass);
		}
		else
		{
			DropOtherMeshFlag = true;
			DropOtherMeshTimer = WeaponSetting.DropOtherMeshSetting.fDropTime;
		}
	}
	
	ChangeDispertionByShot();

	// ������� �������������
	if (WeaponSetting.SoundUseWeapon)
	{
		PlaySoundUseWeapon_Multicast(WeaponSetting.SoundUseWeapon, GetActorLocation());
	}
	if (WeaponSetting.EffectUseWeapon)
	{
		PlayFXUseWeapon_Multicast(WeaponSetting.EffectUseWeapon, ShootLocation->GetComponentTransform());
	}
	

	int8 NumberProjectile = GetNumberProjectileByShoot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotator = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetShootEndLocation();
			//������ ������ � ������������� �������������
			if (ProjectileInfo.ProjectileWeapon)
			{

				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();
				FMatrix MyMatrix(Dir, FVector(0.f, 1.f, 0.f), FVector(0.f, 0.f, 1.f), FVector::ZeroVector);

				SpawnRotator = MyMatrix.Rotator();

				FActorSpawnParameters SpawnParam;
				SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParam.Owner = GetOwner();
				SpawnParam.Instigator = GetInstigator();


				AProjectileWeaponBase* MyProjectile = Cast<AProjectileWeaponBase>(GetWorld()->SpawnActor(ProjectileInfo.ProjectileWeapon, &SpawnLocation, &SpawnRotator, SpawnParam));
				if (MyProjectile)
				{
					MyProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			//������ ������ �������, � ������� ��������������� �������� �� ��������� �� ��������� � ��������� �����
			else
			{
				// �������� ������
				// ����������� FX ��� ������� �� �������
				FHitResult Hit;
				TArray<AActor*> Actor;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.fTraceDistance, ETraceTypeQuery::TraceTypeQuery4, false, Actor,
					EDrawDebugTrace::None, Hit, true, FLinearColor::White, FLinearColor::Blue, 0.5f);
			

				if (WeaponSetting.OtherUsingWeaponFX)
				{
					FRotator EmittorRotation = UKismetMathLibrary::FindLookAtRotation(ShootLocation->GetComponentLocation(), EndLocation * WeaponSetting.fTraceDistance);
					SpawnTraceEmmitor_Multicast(WeaponSetting.OtherUsingWeaponFX, EmittorRotation);
				
				}
		
				// ������� ��� ��������� �� ���������
				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(MySurfaceType))
					{
						UMaterialInterface* MyMaterial = WeaponSetting.ProjectileSetting.HitDecals[MySurfaceType];
						if (MyMaterial && Hit.GetComponent())
						{
							SpawnHitDecal_Multicast(MyMaterial, Hit.GetComponent(), Hit);
							
						}
					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(MySurfaceType))
					{
						UParticleSystem* MyParticle = WeaponSetting.ProjectileSetting.HitFXs[MySurfaceType];
						if (MyParticle)
						{
							SpawnHitFX_Multicast(MyParticle, Hit);
						}
					}
					
					UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, MySurfaceType);
				}
				if (WeaponSetting.ProjectileSetting.ProjectileHitSound)
				{
					SpawnHitSound_Multicast(WeaponSetting.ProjectileSetting.ProjectileHitSound, Hit);
					
				}

				UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.fTraceWeaponDamage, GetInstigatorController(), this, NULL);
				UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), WeaponSetting.fTraceWeaponDamage, Hit.Location, Hit.Location);
				
			}
		}
	}

	if (GetWeaponRound() <= 0 && !bWeaponCooldown)
	{
		if (CanReuse())
		{
			OnWeapopUseEnd.Broadcast(true);
			InitCooldown();
		}
	}
}

void AWeaponBase::SpawnTraceEmmitor_Multicast_Implementation(UParticleSystem* FXTrace, FRotator EmittorRotation)
{
	TraceEmittor = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXTrace, ShootLocation->GetComponentLocation(), EmittorRotation);
	
	DestroyTrace = true;
}

void AWeaponBase::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult Hit)
{
	UGameplayStatics::SpawnDecalAttached(Decal, FVector(20.f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AWeaponBase::SpawnHitFX_Multicast_Implementation(UParticleSystem* FX, FHitResult Hit)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
}

void AWeaponBase::SpawnHitSound_Multicast_Implementation(USoundBase* Sound, FHitResult Hit)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Hit.ImpactPoint);
}

void AWeaponBase::FinishCooldownToUse()
{
	bWeaponCooldown = false;
	int8 AvAmmoFromInventory = GetAviableAmmoToReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - WeaponInfo.Round;
	if (NeedToReload > AvAmmoFromInventory)
	{
		WeaponInfo.Round = WeaponInfo.Round+AvAmmoFromInventory;
		AmmoNeedTakeFromInv = AvAmmoFromInventory;
	}
	else
	{
		WeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeapopReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);


}

void AWeaponBase::CancelReload()
{
	bWeaponCooldown = false;
	DropMeshFlag = false;
	DropOtherMeshFlag = false;
	OnWeapopReloadEnd.Broadcast(false, 0);
}

// ������

// State UPD

//��������� ���������� ��������, � ��� �� ��������� ����� ����� ������������� ������ ��� �������
void AWeaponBase::UpdateStateWeapon_OnServer_Implementation(EMovementState MovementState)
{
	bBlockUse = false;

	switch (MovementState)
	{
	case EMovementState::AimState:
		fCurrentDispersionMax = WeaponSetting.DispersionWeapon.fAim_StateDispersionAimMax;
		fCurrentDispersionMin = WeaponSetting.DispersionWeapon.fAim_StateDispersionAimMin;
		fCurrentDispersionRecoil = WeaponSetting.DispersionWeapon.fAim_StateDispersionAimRecoil;
		fCurrentDispersionReduction = WeaponSetting.DispersionWeapon.fAim_StateDispersionAimReduction;
		break;
	case EMovementState::WalkState:
		fCurrentDispersionMax = WeaponSetting.DispersionWeapon.fWalk_StateDispersionAimMax;
		fCurrentDispersionMin = WeaponSetting.DispersionWeapon.fWalk_StateDispersionAimMin;
		fCurrentDispersionRecoil = WeaponSetting.DispersionWeapon.fWalk_StateDispersionAimRecoil;
		fCurrentDispersionReduction = WeaponSetting.DispersionWeapon.fWalk_StateDispersionAimReduction;
		break;
	case EMovementState::RunState:
		fCurrentDispersionMax = WeaponSetting.DispersionWeapon.fRun_StateDispersionAimMax;
		fCurrentDispersionMin = WeaponSetting.DispersionWeapon.fRun_StateDispersionAimMin;
		fCurrentDispersionRecoil = WeaponSetting.DispersionWeapon.fRun_StateDispersionAimRecoil;
		fCurrentDispersionReduction = WeaponSetting.DispersionWeapon.fRun_StateDispersionAimReduction;
		break;
	case EMovementState::SprintState:
		bBlockUse = true;
		SetWeaponStateFire_OnServer(false);
		break;
	default:
		break;
	}
}

void AWeaponBase::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanUsed())
	{
		bWeaponUse = bIsFire;
	}
	else
	{
		bWeaponUse = false;
		SpeedRate = 0.01f;
	}
}

FVector AWeaponBase::ApplyDisparcionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

void AWeaponBase::ChangeDispertionByShot()
{
	fCurrentDispersion = fCurrentDispersion + fCurrentDispersionRecoil;
}

bool AWeaponBase::CanReuse()
{
	bool result = true;
	if (GetOwner())
	{
		auto Inventory = Cast<UCOVInventoryCharacter>(GetOwner()->GetComponentByClass(UCOVInventoryCharacter::StaticClass()));
		if (Inventory)
		{
			int8 AviableAmmoToReload;
			
			if(!Inventory->CheckAmmoForWeapon(WeaponSetting.WeaponType,  AviableAmmoToReload))
			{
				result = false;
			//	Inventory->OnWeaponNotHaveRound.Broadcast(Inventory->GetWeaponIndexSlotByName(IdWeaponName));
		//	}
		//	else
		//	{
		//		Inventory->OnWeaponHaveRound.Broadcast(Inventory->GetWeaponIndexSlotByName(IdWeaponName));
			}
		}
	}
	return result;
}

int8 AWeaponBase::GetAviableAmmoToReload()
{
	int8 AviableAmmoToReload = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		auto Inventory = Cast<UCOVInventoryCharacter>(GetOwner()->FindComponentByClass(UCOVInventoryCharacter::StaticClass()));
		if (Inventory)
		{
			if (Inventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoToReload))
			{
				//���� ��� !Inventory
				AviableAmmoToReload = AviableAmmoToReload;
				
			}
		}
	}
	if(DebugWeaponShow)
		UE_LOG(LogTemp, Warning, TEXT("%i"), AviableAmmoToReload);
	return AviableAmmoToReload;
}

// State UPD

// GetSome...()

FVector AWeaponBase::GetShootEndLocation()
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);
	FVector tpmV = (ShootLocation->GetComponentLocation() - EndLocationShoot);

	if (tpmV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDisparcionToShoot((ShootLocation->GetComponentLocation() - EndLocationShoot).GetSafeNormal()) * -10000.f;
		if (DebugWeaponShow)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - EndLocationShoot), WeaponSetting.fTraceDistance, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);

		}
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDisparcionToShoot(ShootLocation->GetForwardVector()) * 10000.0f;
		if (DebugWeaponShow)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.fTraceDistance, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);

		}
	}
	if (DebugWeaponShow)
	{
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 1.0f);
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocationShoot, FColor::Red, false, 5.f, (uint8)'\000', 1.0f);
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Red, false, 5.f, (uint8)'\000', 1.0f);
		DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.f);

	}


	return EndLocation;
}

bool AWeaponBase::CheckWeaponCanUsed()
{
	if (GetWeaponRound() <= 0)
	{
		bBlockUse = true;
	}
	else
		bBlockUse = false;
	return !bBlockUse;
}

FProjectileInfo AWeaponBase::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

float AWeaponBase::GetCurrentDispersion() const
{
	float Result = fCurrentDispersion;
	return Result;
}

int8 AWeaponBase::GetNumberProjectileByShoot() const
{
	return WeaponSetting.NumberProjectileByShoot;
}

int32 AWeaponBase::GetWeaponRound()
{
	return WeaponInfo.Round;
}

// GetSome...()



void AWeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
		DOREPLIFETIME(AWeaponBase, WeaponInfo);
		DOREPLIFETIME(AWeaponBase, EndLocationShoot);
		DOREPLIFETIME(AWeaponBase, bWeaponCooldown);
}

void AWeaponBase::PlaySoundUseWeapon_Multicast_Implementation(USoundBase* Sound, FVector Location)
{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Location);
}

void AWeaponBase::PlayFXUseWeapon_Multicast_Implementation(UParticleSystem* FX, FTransform Transform)
{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, Transform);
}

void AWeaponBase::InitShellDrop_Multicast_Implementation(UStaticMesh* MeshToDrop, FTransform Offset, FVector ImpulseDrop, float LifeTime, float ImpulseRandomDespersion, float PowerImpulse, float CustomMass, FVector LocalDir)
{
	FActorSpawnParameters SpawnParam;
	SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnParam.Owner = this;
	AStaticMeshActor* NewActor = nullptr;
	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, SpawnParam);
	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		auto Component = NewActor->GetStaticMeshComponent();

		Component->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		Component->SetCollisionResponseToChannel(ECC_GameTraceChannel11, ECollisionResponse::ECR_Ignore);
		Component->SetCollisionResponseToChannel(ECC_GameTraceChannel12, ECollisionResponse::ECR_Ignore);
		Component->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		Component->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		Component->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);
		Component->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

		Component->Mobility = EComponentMobility::Movable;
		Component->SetStaticMesh(MeshToDrop);
		Component->SetSimulatePhysics(true);
		NewActor->SetActorTickEnabled(false);
		NewActor->SetLifeSpan(LifeTime);

		if (CustomMass > 0.f)
		{
			Component->SetMassOverrideInKg(NAME_None, CustomMass, true);
		}
		if (!ImpulseDrop.IsNearlyZero())
		{
			FVector FinalDir;
			LocalDir = LocalDir + ImpulseDrop * 1000.f;
			if (!FMath::IsNearlyZero(ImpulseRandomDespersion))
			{
				FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDespersion);
				FinalDir.GetSafeNormal(0.001f);
				Component->AddImpulse(FinalDir * PowerImpulse);
			}
		}
	}
}


void AWeaponBase::UpdateWeaponEndLocationByCharacter_OnServer_Implementation(FVector EndLocation, bool NewShouldReduceDespersion)
{
	EndLocationShoot = EndLocation;
	bShouldReduceDispersion = NewShouldReduceDespersion;
}


