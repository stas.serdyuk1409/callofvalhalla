// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Weapon/Projectile/ProjectileWeaponBase.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Perception/AISense_Damage.h"

int32 DebugProjectileShow = 0;
FAutoConsoleVariableRef CVarProjectileShow(
	TEXT("COV.DebugProjectile"),
	DebugProjectileShow,
	TEXT("Draw Debug for Projectile"),
	ECVF_Cheat);



// Sets default values
AProjectileWeaponBase::AProjectileWeaponBase()
{
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	RootComponent = SphereCollision;
	SphereCollision->SetSphereRadius(16.f);
	SphereCollision->SetCollisionProfileName("Projectile");

	SphereCollision->bReturnMaterialOnMove = true;
	SphereCollision->SetCanEverAffectNavigation(false);


	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	StaticMeshComponent->SetupAttachment(SphereCollision);
	StaticMeshComponent->SetCanEverAffectNavigation(false);

	ProjectileFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile FX"));
	ProjectileFX->SetupAttachment(SphereCollision);
	
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovementComponent -> UpdatedComponent = RootComponent;
	ProjectileMovementComponent->InitialSpeed = ProjectileSetting.fProjectileInitSpeed;
	ProjectileMovementComponent->MaxSpeed = ProjectileSetting.fProjectileInitSpeed;

	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void AProjectileWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	SphereCollision->OnComponentHit.AddDynamic(this, &AProjectileWeaponBase::ProjectileCollisionSphereHit);
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AProjectileWeaponBase::ProjectileCollisionSphereBeginOverlap);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &AProjectileWeaponBase::ProjectileCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileWeaponBase::InitProjectile(FProjectileInfo InitParam)
{
	ProjectileSetting = InitParam;
	ProjectileMovementComponent->InitialSpeed = ProjectileSetting.fProjectileInitSpeed;
	ProjectileMovementComponent->MaxSpeed = ProjectileSetting.fProjectileInitSpeed;
	ProjectileMovementComponent->ProjectileGravityScale = ProjectileSetting.fProjectileGravityScale;
	this->SetLifeSpan(ProjectileSetting.fProjectileLifeTime);
	if (ProjectileSetting.ProjectileMesh)
	{
		InitVisualMeshProjectile_Multicast(ProjectileSetting.ProjectileMesh, ProjectileSetting.ProjectileMeshTransform);
	}
	else
	{
		StaticMeshComponent->DestroyComponent();
	}
	if (ProjectileSetting.ProjectileTrialFX)
	{
		InitVisualTrailProjectile_Multicast(ProjectileSetting.ProjectileTrialFX, ProjectileSetting.ProjectileTrialFXTransform);
	}
	else
	{
		ProjectileFX->DestroyComponent();
	}
	

}

void AProjectileWeaponBase::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
		if (ProjectileSetting.HitDecals.Contains(MySurfaceType))
		{
			UMaterialInterface* MyMaterial = ProjectileSetting.HitDecals[MySurfaceType];
			if (MyMaterial && OtherComp)
			{
				SpawnHitDecal_Multicast(MyMaterial, OtherComp, Hit);
			}
		}
		if (ProjectileSetting.HitFXs.Contains(MySurfaceType))
		{
			UParticleSystem* MyParticle = ProjectileSetting.HitFXs[MySurfaceType];
			if (MyParticle)
			{
				SpawnHitFX_Multicast(MyParticle, Hit);
			}
		}
	
			UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileSetting.Effect, MySurfaceType);
		
	}
	if (ProjectileSetting.ProjectileHitSound)
	{
		SpawnHitSound_Multicast(ProjectileSetting.ProjectileHitSound, Hit);
	}
	UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.fProjectileDamage, GetInstigatorController(), this, NULL);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.fProjectileDamage, Hit.Location, Hit.Location);
	ImpactProjectile();
}

void AProjectileWeaponBase::ProjectileCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AProjectileWeaponBase::ProjectileCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileWeaponBase::ImpactProjectile()
{
	this->Destroy();
}

void AProjectileWeaponBase::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult Hit)
{
	UGameplayStatics::SpawnDecalAttached(Decal, FVector(20.f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AProjectileWeaponBase::SpawnHitFX_Multicast_Implementation(UParticleSystem* FX, FHitResult Hit)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));

}

void AProjectileWeaponBase::SpawnHitSound_Multicast_Implementation(USoundBase* Sound, FHitResult Hit)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Hit.ImpactPoint);
}

void AProjectileWeaponBase::InitVisualMeshProjectile_Multicast_Implementation(UStaticMesh* NewMesh, FTransform NewTransform)
{
	StaticMeshComponent->SetStaticMesh(NewMesh);
	StaticMeshComponent->SetRelativeTransform(NewTransform);
}

void AProjectileWeaponBase::InitVisualTrailProjectile_Multicast_Implementation(UParticleSystem* NewEmmitter, FTransform NewTransform)
{
	ProjectileFX->SetTemplate(NewEmmitter);
	ProjectileFX->SetRelativeTransform(NewTransform);
}

