// Copyright ICFHAG Studio. All rights reserved. 2021


#include "FuncLibrary/Types.h"
#include "../CallOfValhalla.h"
#include "../Public/COVInterfaceGameActor.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UCOVStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	
	
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UCOVStateEffect* myEffect = Cast<UCOVStateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractableSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractableSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UCOVStateEffect*> CurrentEffects;
						ICOVInterfaceGameActor* myInterface = Cast<ICOVInterfaceGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}

					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{

						UCOVStateEffect* NewEffect = NewObject<UCOVStateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							
							FName Spine = "spine_02";
							NewEffect->InitObject(TakeEffectActor, Spine);
						}
					}

				}
				i++;
			}
		}

	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* Target, FVector Offset, FName Socket)
{
	if (Target)
	{
		FName SocketToAttach = Socket;
		FVector Loc = Offset;
		ACharacter* MyCharacter = Cast<ACharacter>(Target);
		if (MyCharacter && MyCharacter->GetMesh())
		{
			UGameplayStatics::SpawnEmitterAttached(ExecuteFX, MyCharacter->GetMesh(), SocketToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			if (Target->GetRootComponent())
			{
				UGameplayStatics::SpawnEmitterAttached(ExecuteFX, Target->GetRootComponent(), SocketToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
}
