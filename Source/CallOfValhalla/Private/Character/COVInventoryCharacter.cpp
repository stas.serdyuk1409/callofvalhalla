// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Character/COVInventoryCharacter.h"
#include "../Public/Game/COVGameInstance.h"
#include "Engine/StaticMeshActor.h"
#include "../Public/COVInterfaceGameActor.h"
#include "Net/UnrealNetwork.h"
// Sets default values for this component's properties
UCOVInventoryCharacter::UCOVInventoryCharacter()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UCOVInventoryCharacter::BeginPlay()
{
	Super::BeginPlay();

	
}


// Called every frame
void UCOVInventoryCharacter::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//����������� ��������� �� ������ � ����������� � ������ �������
bool UCOVInventoryCharacter::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAddicionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponsSlot.Num() - 1)
		CorrectIndex = 0;
	else
		if (ChangeToIndex < 0)
			CorrectIndex = WeaponsSlot.Num() - 1;

	FName NewIdWeapon;
	FAddicionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponsSlot.IsValidIndex(CorrectIndex))
	{
		if (!WeaponsSlot[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponsSlot[CorrectIndex].AddicionalWeaponInfo.Round > 0)
			{
				//good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UCOVGameInstance* myGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//check ammoSlots for this weapon
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponsSlot[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponsSlot[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponsSlot[CorrectIndex].AddicionalWeaponInfo;
			}
		}
	}
	if (!bIsSuccess)
	{
		int8 iteration = 0;
		int8 Seconditeration = 0;
		int8 tmpIndex = 0;
		while (iteration < WeaponsSlot.Num() && !bIsSuccess)
		{
			iteration++;

			if (bIsForward)
			{
				//Seconditeration = 0;

				tmpIndex = ChangeToIndex + iteration;
			}
			else
			{
				Seconditeration = WeaponsSlot.Num() - 1;

				tmpIndex = ChangeToIndex - iteration;
			}

			if (WeaponsSlot.IsValidIndex(tmpIndex))
			{
				if (!WeaponsSlot[tmpIndex].NameItem.IsNone())
				{
					if (WeaponsSlot[tmpIndex].AddicionalWeaponInfo.Round > 0)
					{
						//WeaponGood
						bIsSuccess = true;
						NewIdWeapon = WeaponsSlot[tmpIndex].NameItem;
						NewAdditionalInfo = WeaponsSlot[tmpIndex].AddicionalWeaponInfo;
						NewCurrentIndex = tmpIndex;
					}
					else
					{
						FWeaponInfo myInfo;
						UCOVGameInstance* myGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());

						myGI->GetWeaponInfoByName(WeaponsSlot[tmpIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponsSlot[tmpIndex].NameItem;
								NewAdditionalInfo = WeaponsSlot[tmpIndex].AddicionalWeaponInfo;
								NewCurrentIndex = tmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else
			{
				//go to end of LEFT of array weapon slots
				if (OldIndex != Seconditeration)
				{
					if (WeaponsSlot.IsValidIndex(Seconditeration))
					{
						if (!WeaponsSlot[Seconditeration].NameItem.IsNone())
						{
							if (WeaponsSlot[Seconditeration].AddicionalWeaponInfo.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponsSlot[Seconditeration].NameItem;
								NewAdditionalInfo = WeaponsSlot[Seconditeration].AddicionalWeaponInfo;
								NewCurrentIndex = Seconditeration;
							}
							else
							{
								FWeaponInfo myInfo;
								UCOVGameInstance* myGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponsSlot[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponsSlot[Seconditeration].NameItem;
										NewAdditionalInfo = WeaponsSlot[Seconditeration].AddicionalWeaponInfo;
										NewCurrentIndex = Seconditeration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					//go to same weapon when start
					if (WeaponsSlot.IsValidIndex(Seconditeration))
					{
						if (!WeaponsSlot[Seconditeration].NameItem.IsNone())
						{
							if (WeaponsSlot[Seconditeration].AddicionalWeaponInfo.Round > 0)
							{
								//WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo myInfo;
								UCOVGameInstance* myGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponsSlot[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
									{
										if (AmmoSlots[j].Cout > 0)
										{
											//WeaponGood, it same weapon do nothing
										}
										else
										{
											//Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error, TEXT("Iventory::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					Seconditeration++;
				}
				else
				{
					Seconditeration--;
				}

			}
		}
	}
	if (bIsSuccess)
	{
		SetAddicionalWeaponInfo(OldIndex, OldInfo);
		OnSwitchWeapon_OnServer(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
		
	}

	return bIsSuccess;
}

FAddicionalWeaponInfo UCOVInventoryCharacter::GetFAddicionalWeaponInfo(int32 IndexWeapon)
{
	FAddicionalWeaponInfo result;
	if (WeaponsSlot.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponsSlot.Num() && !bIsFind)
		{
			if(i == IndexWeapon)
			{
				result = WeaponsSlot[i].AddicionalWeaponInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{

		}
	}

	return result;
}

int32 UCOVInventoryCharacter::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result =-1;
	bool bIsFind = false;
	int8 i = 0;
	while (i < WeaponsSlot.Num() && !bIsFind)
	{
			if (WeaponsSlot[i].NameItem == IdWeaponName)
			{
				result = i;
				bIsFind = true;
			}
			i++;
	}
	

	return result;
}

void UCOVInventoryCharacter::SetAddicionalWeaponInfo(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo)
{
	if (WeaponsSlot.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponsSlot.Num() && !bIsFind)
		{
			if ( i == IndexWeapon)
			{
				WeaponsSlot[i].AddicionalWeaponInfo = NewInfo;
				bIsFind = true;
				OnWeaponAdditionalInfoChange_Multicast(IndexWeapon, NewInfo);
				
			}
			i++;
		}
		if (!bIsFind)
		{

		}
	}

}

void UCOVInventoryCharacter::WeaponChangeAmmo(EWeaponType TypeWeapon, int32 AmmoChange)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i<AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += AmmoChange;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
			{
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}
			//mb here
			OnAmmoChange_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			
			bIsFind = true;
		}
		i++;
	}
	
}

bool UCOVInventoryCharacter::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AviableAmmoToReload)
{
	AviableAmmoToReload = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoToReload = AmmoSlots[i].Cout;
			if (AmmoSlots[i].Cout > 0)
			{
			
				
				return true;
			}
		}
		i++;
		
	}
	if (AviableAmmoToReload <= 0)
		OnAmmoWeaponEmpty_Multicast(TypeWeapon);
	else
		OnAmmoWeaponAvaible_Multicast(TypeWeapon);
		

	return false;
}

bool UCOVInventoryCharacter::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAddicionalWeaponInfo PreviosWeaponInfo)
{
	bool bIsSuccess = false;
	FName ToSwitchIdWeapon;
	FAddicionalWeaponInfo ToSwitchAdditionalInfo;

	ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
	ToSwitchAdditionalInfo = GetFAddicionalWeaponInfo(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAddicionalWeaponInfo(PreviosIndex, PreviosWeaponInfo);
		OnSwitchWeapon_OnServer(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);
	

		//check ammo slot for event to player		
		EWeaponType ToSwitchWeaponType;
		if (GetWeaponTypeBySlotIndex(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int8 AviableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
			{

			}
		}
		bIsSuccess = true;
	}
	return bIsSuccess;
}

bool UCOVInventoryCharacter::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponsSlot.Num() && !bIsFreeSlot)
	{
		if (WeaponsSlot[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UCOVInventoryCharacter::CheckWeaponNameInSlot(FWeaponSlot NewWeapon)
{
	bool bCanPick = true;
	int8 i = 0;
	while (i < WeaponsSlot.Num() && bCanPick)
	{
		if (NewWeapon.NameItem == WeaponsSlot[i].NameItem)
		{
			bCanPick = false;
		}
		i++;
	}
	return bCanPick;
}

bool UCOVInventoryCharacter::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
		{
			result = true;
		}
		i++;
	}
	return result;
}

bool UCOVInventoryCharacter::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
	bool result = false;

	bool bCanPick = true;
	int8 i = 0;
	while (i < WeaponsSlot.Num() && bCanPick)
	{
		if (NewWeapon.NameItem == WeaponsSlot[i].NameItem)
		{
			bCanPick = false;
		}
		i++;
	}
	if (bCanPick)
	{

		if (WeaponsSlot.IsValidIndex(IndexSlot) && GetDropItemFromInventory(IndexSlot, DropItemInfo))
		{
			WeaponsSlot[IndexSlot] = NewWeapon;
			SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AddicionalWeaponInfo, true);
			OnUpdateWeaponSlot_Multicast(IndexSlot, NewWeapon);
			

			//��� �����������
			FWeaponInfo WeaponInfo;
			FWeaponSlot TryWeapon;
			UCOVGameInstance* MyGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());
			if (MyGI)
			{
				MyGI->GetWeaponInfoByName(NewWeapon.NameItem, WeaponInfo);
				if (NewWeapon.AddicionalWeaponInfo.Round > WeaponInfo.MaxRound)
				{
					TryWeapon.AddicionalWeaponInfo.Round = WeaponInfo.MaxRound;
					TryWeapon.NameItem = NewWeapon.NameItem;
					OnWeaponAdditionalInfoChange_Multicast(IndexSlot, TryWeapon.AddicionalWeaponInfo);
				}
				else
				{
					OnWeaponAdditionalInfoChange_Multicast(IndexSlot, NewWeapon.AddicionalWeaponInfo);
				}

			}
			// 

			result = true;
		}
	}
	return result;
}


FName UCOVInventoryCharacter::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName NameWeapon;
	if (WeaponsSlot.IsValidIndex(IndexSlot))
	{
		NameWeapon = WeaponsSlot[IndexSlot].NameItem;
	}
	return NameWeapon;
}

bool UCOVInventoryCharacter::GetWeaponTypeBySlotIndex(FName IdWeaponName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::Bow;
	UCOVGameInstance* myGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		myGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	return false;
}

void UCOVInventoryCharacter::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& InitWeaponsSlot, const TArray<FAmmoSlot>& InitAmmoSlots)
{
	WeaponsSlot = InitWeaponsSlot;
	AmmoSlots = InitAmmoSlots;
	MaxSlotWeapon = InitWeaponsSlot.Num();
	FWeaponInfo WeaponInfo;
	UCOVGameInstance* MyGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		for (int8 i = 0; i < WeaponsSlot.Num(); i++)
		{
			if (!WeaponsSlot[i].NameItem.IsNone())
			{
						
			}
		}

	}
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponsSlot.Num() && !bIsFind)
	{
			if (WeaponsSlot.IsValidIndex(0))
			{
				if (!WeaponsSlot[i].NameItem.IsNone())
				{
					SwitchWeaponToIndex(i, i - 1, WeaponsSlot[i].AddicionalWeaponInfo, true);
					OnSwitchWeapon_OnServer(WeaponsSlot[i].NameItem, WeaponsSlot[i].AddicionalWeaponInfo, i);
				
					bIsFind = true;
				}
			
			}
		i++;
	}

}

void UCOVInventoryCharacter::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickUpActor, FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;

	bool bCanPick = true;

	
	if (CheckCanTakeWeapon(IndexSlot))
	{
		bCanPick = CheckWeaponNameInSlot(NewWeapon);
		if (bCanPick)
		{
			if (WeaponsSlot.IsValidIndex(IndexSlot))
			{ //��� �����������
				WeaponsSlot[IndexSlot] = NewWeapon;
				FWeaponInfo WeaponInfo;
				FWeaponSlot TryWeapon;
				UCOVGameInstance* MyGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());
				if (MyGI)
				{
					MyGI->GetWeaponInfoByName(NewWeapon.NameItem, WeaponInfo);
					if (NewWeapon.AddicionalWeaponInfo.Round > WeaponInfo.MaxRound)
					{
						TryWeapon.AddicionalWeaponInfo.Round = WeaponInfo.MaxRound;
						TryWeapon.NameItem = NewWeapon.NameItem;
						OnUpdateWeaponSlot_Multicast(IndexSlot, NewWeapon);
						OnWeaponAdditionalInfoChange_Multicast(IndexSlot, TryWeapon.AddicionalWeaponInfo);
						if (PickUpActor)
							PickUpActor->Destroy();
						
					}
					else
					{
						//realy???
						OnUpdateWeaponSlot_Multicast(IndexSlot, NewWeapon);
						OnWeaponAdditionalInfoChange_Multicast(IndexSlot, NewWeapon.AddicionalWeaponInfo);
						if (PickUpActor)
							PickUpActor->Destroy();
					}
				
				}
			}
		}
	
	}
	
}
bool UCOVInventoryCharacter::GetDropItemFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool result = false;
	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	auto myGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{

		result = myGI->GetDropInfoByName(DropItemName, DropItemInfo);
		if (WeaponsSlot.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponSlotInfo.AddicionalWeaponInfo = WeaponsSlot[IndexSlot].AddicionalWeaponInfo;
		}

	}
	return result;
}
void UCOVInventoryCharacter::DropWeaponByIndex_OnServer_Implementation(int32 ByIndex)
{
	FWeaponSlot EmtyWeaponSlot;
	FDropItem DropItemInfo;
	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AviableWeaponNum = 0;
	while (i < WeaponsSlot.Num() && !bIsCanDrop)
	{
		if (!WeaponsSlot[i].NameItem.IsNone())
		{
			AviableWeaponNum++;
			if (AviableWeaponNum > 1)
				bIsCanDrop = true;
		}
		i++;
	}

	if (bIsCanDrop && WeaponsSlot.IsValidIndex(ByIndex) && GetDropItemFromInventory(ByIndex, DropItemInfo))
	{
		WeaponsSlot[ByIndex] = EmtyWeaponSlot;
		if (GetOwner()->GetClass()->ImplementsInterface(UCOVInterfaceGameActor::StaticClass()))
		{
			ICOVInterfaceGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}
		OnUpdateWeaponSlot_Multicast(ByIndex, EmtyWeaponSlot);
		

		//switch weapon to valid slot weapon from start weapon slots array
		bool bIsFindWeapon = false;
		int8 j = 0;
		while (j < WeaponsSlot.Num() && !bIsFindWeapon)
		{
			if (!WeaponsSlot[j].NameItem.IsNone())
			{
				OnSwitchWeapon_OnServer(WeaponsSlot[j].NameItem, WeaponsSlot[j].AddicionalWeaponInfo, j);
				
			}
			j++;
		}

	
	}
}

void UCOVInventoryCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCOVInventoryCharacter, WeaponsSlot);
	DOREPLIFETIME(UCOVInventoryCharacter, AmmoSlots);

}



void UCOVInventoryCharacter::OnAmmoChange_Multicast_Implementation(EWeaponType TypeWeapon, int32 Cout)
{
	OnAmmoChange.Broadcast(TypeWeapon, Cout);
}

void UCOVInventoryCharacter::OnWeaponAdditionalInfoChange_Multicast_Implementation(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
}

void UCOVInventoryCharacter::OnAmmoWeaponEmpty_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnAmmoWeaponEmpty.Broadcast(TypeWeapon);
}

void UCOVInventoryCharacter::OnAmmoWeaponAvaible_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnAmmoWeaponAvaible.Broadcast(TypeWeapon);
}

void UCOVInventoryCharacter::OnUpdateWeaponSlot_Multicast_Implementation(int32 IndexSlot, FWeaponSlot NewWeapon)
{
	OnUpdateWeaponSlot.Broadcast(IndexSlot, NewWeapon);
}

void UCOVInventoryCharacter::OnWeaponNotHaveRound_Multicast_Implementation()
{

}

void UCOVInventoryCharacter::OnWeaponHaveRound_Multicast_Implementation()
{

}



void UCOVInventoryCharacter::OnSwitchWeapon_OnServer_Implementation(FName WeaponIdName, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 NewCurrentIndexWeapon)
{
	OnSwitchWeapon.Broadcast(WeaponIdName, AddicionalWeaponInfo, NewCurrentIndexWeapon);
}
#pragma optimize ("", on)