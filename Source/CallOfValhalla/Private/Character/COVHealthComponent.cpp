// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Character/COVHealthComponent.h"
#include "Net/UnrealNetwork.h"
// Sets default values for this component's properties
UCOVHealthComponent::UCOVHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UCOVHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	bIsAlive = true;
	
}


// Called every frame
void UCOVHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UCOVHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UCOVHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

bool UCOVHealthComponent::GetIsAlive()
{
	return bIsAlive;
}

void UCOVHealthComponent::ChangeCurrentHealth_OnServer_Implementation(float ChangeValue)
{
	if (GetCurrentHealth() > 0.f) 
	{
		Health += ChangeValue * fDamageCoef;
		bIsAlive = true;
		OnHealthChange_Multicast(Health, ChangeValue);
		if (Health > 100.f)
		{
			Health = 100.f;
		}
		else
		{
			if (Health <= 0.f)
			{
				bIsAlive = false;
				OnDead_Multicast();
			}
		}
	}
	
	
}

void UCOVHealthComponent::OnDead_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UCOVHealthComponent::OnHealthChange_Multicast_Implementation(float NewHealth, float Damage)
{
	OnHealthChange.Broadcast(NewHealth, Damage);
}



void UCOVHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCOVHealthComponent, Health);
	DOREPLIFETIME(UCOVHealthComponent, bIsAlive);
	
}