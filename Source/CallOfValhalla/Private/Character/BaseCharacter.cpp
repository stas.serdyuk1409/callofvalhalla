// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Character/BaseCharacter.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "../Public/Game/COVGameInstance.h"
#include "../Public/Weapon/WeaponBase.h"
#include "../Public/Character/COVInventoryCharacter.h"
#include "../Public/Character/COVCharacterHealthComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "../Public/Weapon/Projectile/ProjectileWeaponBase.h"
#include "../Public/COVStateEffect.h"
#include "Net/UnrealNetwork.h"
#include "../CallOfValhalla.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"
ABaseCharacter::ABaseCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	
	//Net
	bReplicates = true;
}

void ABaseCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	//bIsAlive = true;
	
	
}

float ABaseCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (CharacterHealthComponent && CharacterHealthComponent->GetIsAlive() || HealthComponent && HealthComponent->GetIsAlive())
	{
		float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		if (HealthComponent)
		{
			HealthComponent->ChangeCurrentHealth_OnServer(-DamageAmount);
		}
		else if (CharacterHealthComponent)
		{
			CharacterHealthComponent->ChangeCurrentHealth_OnServer(-DamageAmount);
		}

		if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
		{
			AProjectileWeaponBase* myProjectile = Cast<AProjectileWeaponBase>(DamageCauser);
			if (myProjectile)
			{
				UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
			}
		}
		return ActualDamage;

	}
	return 0.f;
}
// ������

void ABaseCharacter::InitWeapon(FName IdWeapon, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 Index)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	FWeaponInfo WeaponInfo;

	ReferenceGI = Cast<UCOVGameInstance>(GetWorld()->GetGameInstance());
	if (ReferenceGI)
	{

		if (ReferenceGI->GetWeaponInfoByName(IdWeapon, WeaponInfo))
		{

			if (WeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);
				FActorSpawnParameters SpawnParam;
				SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParam.Owner = this;
				SpawnParam.Instigator = GetInstigator();

				AWeaponBase* MyWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParam));

				if (MyWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);

					// ���� ������� ������ (�����/������ ����)
					if (WeaponInfo.RightHand == true || WeaponInfo.MeleeWeapon == true)
					{
						MyWeapon->AttachToComponent(GetMesh(), Rule, FName("RightHandSocket"));
						bRightHandState = true;
					}
					else
					{
						MyWeapon->AttachToComponent(GetMesh(), Rule, FName("LeftHandSocket"));
						bRightHandState = false;
					}

					CurrentWeapon = MyWeapon;
					bMeleeWeaponState = WeaponInfo.MeleeWeapon;
					CurrentWeaponIndex = Index;
					
					MyWeapon->WeaponSetting = WeaponInfo;
					MyWeapon->IdWeaponName = IdWeapon;
					if (MyWeapon->WeaponSetting.MaxRound >= AddicionalWeaponInfo.Round)
						MyWeapon->WeaponInfo = AddicionalWeaponInfo;
					else
						MyWeapon->WeaponInfo.Round = MyWeapon->WeaponSetting.MaxRound;

					MyWeapon->UpdateStateWeapon_OnServer(MovementState);
					/* �������� � ��������� */ {
						MyWeapon->OnWeapopReloadStart.AddDynamic(this, &ABaseCharacter::WeaponReloadStart_Multicast);
						MyWeapon->OnWeapopReloadEnd.AddDynamic(this, &ABaseCharacter::WeaponReloadEnd_Multicast);
						MyWeapon->OnWeapopUseStart.AddDynamic(this, &ABaseCharacter::WeaponUsedStart_Multicast);
						MyWeapon->OnWeapopUseEnd.AddDynamic(this, &ABaseCharacter::WeaponUseEnd);
						if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CanReuse())
						{
							CurrentWeapon->InitCooldown();
						}


					}

				}
			}
		}


	}
}
void ABaseCharacter::CharacterAttackEvent(bool bIsAttacking)
{
		AWeaponBase* MyWeapon = nullptr;
		MyWeapon = GetCurrentWeapon();
		if (MyWeapon)
		{
			MyWeapon->SetWeaponStateFire_OnServer(bIsAttacking);
		}
}

TArray<UCOVStateEffect*> ABaseCharacter::GetAllEffectOnPawn()
{
	return Effects;
}



// ������

// Change State

void ABaseCharacter::CharackterUpdate()
{
	float ResultSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::AimState:
		ResultSpeed = SpeedState.AimSpeed;
		break;
	case EMovementState::WalkState:
		ResultSpeed = SpeedState.WalkSpeed;
		break;
	case EMovementState::RunState:
		ResultSpeed = SpeedState.RunSpeed;
		break;
	case EMovementState::SprintState:
		ResultSpeed = SpeedState.SprintSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ABaseCharacter::ChangeMovementState()
{
	EMovementState CurrentState = EMovementState::RunState;
	if (!bWalkState && !bSprintState && !bAimState)
	{
		CurrentState = EMovementState::RunState;
	}
	else
	{
		if (bSprintState)
		{
			bWalkState = false;
			bAimState = false;
			CurrentState = EMovementState::SprintState;
		}
		else
		{
			if (bWalkState && !bSprintState && !bAimState)
			{
				CurrentState = EMovementState::WalkState;
			}
			else if (bAimState)
			{
				bWalkState = false;
				CurrentState = EMovementState::AimState;
			}
		}
	}
	/*CharackterUpdate();*/
	AWeaponBase* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->UpdateStateWeapon_OnServer(CurrentState);
	}
	SetMovementState_OnServer(CurrentState);
}

// Change State

// GetSome...()

AWeaponBase* ABaseCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ABaseCharacter::EnableRackdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);

		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

// GetSome...()

//��������



void ABaseCharacter::WeaponReloadStart_Multicast_Implementation(UAnimMontage* AnimReload, float ReUseTime)
{
	
	if (ReUseTime <= 1.f)
	{
		if(AnimReload && ReUseTime)
		PlayAnimMontage(AnimReload, 1.0f / (ReUseTime));
	}
	else
	{
		if (AnimReload && ReUseTime)
		PlayAnimMontage(AnimReload, (ReUseTime - 1.f) / (ReUseTime + 1.f));
	}
	bReUseWeapon = true;
	if(GetController() && GetController()->IsLocalPlayerController())
	WeaponReloadStart_BP();
}

void ABaseCharacter::WeaponReloadEnd_Multicast_Implementation(bool IsSuccess, int32 AmmoSafe)
{
	if (IsSuccess)
	{
		bReUseWeapon = false;
		
	}
	else
	{
		StopAnimMontage();
		bReUseWeapon = false;
	}
	if (GetController() && GetController()->IsLocalPlayerController())
	WeaponReloadEnd_BP(IsSuccess);
	AdditionalActionForWeaponReloadEnd(IsSuccess, AmmoSafe);
}



void ABaseCharacter::WeaponUsedStart_Multicast_Implementation(UAnimMontage* AnimUsingWeapon, float UsingTime)
{
	if (UsingTime <= 1.f)
	{
		if(AnimUsingWeapon && UsingTime)
		PlayAnimMontage(AnimUsingWeapon, 1.0f / (UsingTime));
	}
	else
	{
		if (AnimUsingWeapon && UsingTime)
		PlayAnimMontage(AnimUsingWeapon, (UsingTime - 1.f) / (UsingTime + 1.f));
	}
	bUseWeapon = true;
	AdditionalActionForWeaponUse();
}

void ABaseCharacter::WeaponReloadStart_BP_Implementation()
{
}
void ABaseCharacter::WeaponReloadEnd_BP_Implementation(bool Success)
{
}
void ABaseCharacter::DeathCharacter_BP_Implementation()
{
}
void ABaseCharacter::AdditionalActionForWeaponUse()
{
	//Implement in child
}
void ABaseCharacter::AdditionalActionForWeaponReloadEnd(bool IsSucces, int32 AmmoSafe)
{
	//Implement in child
}
void ABaseCharacter::WeaponUseEnd(bool IsSuccess)
{
	bUseWeapon = false;
}

void ABaseCharacter::DeathCharacter()
{
	DeathCharacter_BP();
	if (HasAuthority())
	{
		//if(HealthComponent -> bIsAlive = false;

		int32 RandomInt = FMath::RandHelper(DeathMontage.Num());
		if (DeathMontage.IsValidIndex(RandomInt) && DeathMontage[RandomInt] && GetMesh()->GetAnimInstance())
		{
			TimeDeathAnim = DeathMontage[RandomInt]->GetPlayLength();
			GetMesh()->GetAnimInstance()->StopAllMontages(0.2f);
			PlayAnim_Multicast(DeathMontage[RandomInt]);

		}
		else
			UE_LOG(LogTemp, Warning, TEXT("ABaseCharacter::DeathCharacter error"));
		if (GetController())
			GetController()->UnPossess();
		//UnPossessed();

		
		GetWorldTimerManager().SetTimer(TimerHandle_Rackdoll, this, &ABaseCharacter::EnableRackdoll_Multicast, TimeDeathAnim, false);
	}
	else
	{
		CharacterAttackEvent(false);
	}

	if (GetCapsuleComponent())
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

	
}

EPhysicalSurface ABaseCharacter::GetSurfaceType()
{
	EPhysicalSurface SurfaceResult = EPhysicalSurface::SurfaceType_Default;
	if (CharacterHealthComponent)
	{
		if (CharacterHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					SurfaceResult = myMaterial->GetPhysicalMaterial()->SurfaceType;

				}
			}
		}
	}
	else if (HealthComponent)
	{
		if (GetMesh())
		{
			UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
			if (myMaterial)
			{
				SurfaceResult = myMaterial->GetPhysicalMaterial()->SurfaceType;

			}
		}
	}

	return SurfaceResult;
}

TArray<UCOVStateEffect*> ABaseCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ABaseCharacter::RemoveEffect(UCOVStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bAutoDestroy)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ABaseCharacter::AddEffect(UCOVStateEffect* newEffect)
{
	Effects.Add(newEffect);
	if (!newEffect->bAutoDestroy)
	{
		SwitchEffect(newEffect, true);
		EffectAdd = newEffect;
	}
	else
	{
		if (newEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(newEffect->ParticleEffect);
		}
	}
	
}
void ABaseCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ABaseCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}
void ABaseCharacter::SwitchEffect(UCOVStateEffect* Effect, bool bAddOrRemove)
{
	if (bAddOrRemove)
	{
		if (Effect&& Effect->ParticleEffect)
		{
			FName BoneToAttached = Effect->BoneToAttachedEffect;
			FVector Loc = FVector(0);
			USkeletalMeshComponent* MeshSkelet = GetMesh();
			if (MeshSkelet)
			{
				UParticleSystemComponent* Partical = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MeshSkelet, BoneToAttached, Loc, FRotator::ZeroRotator, Effect->ScaleEmitter, EAttachLocation::SnapToTarget, false);
				ParticalEffects.Add(Partical);
			}
		}
	}
	else
	{
		if (ParticalEffects.Num() > 0)
		{
			int32 i = 0;
			bool bIsFind = false;
			while (i < ParticalEffects.Num() && !bIsFind)
			{
				if (ParticalEffects[i] && ParticalEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticalEffects[i]->Template)
				{
					bIsFind = true;
					ParticalEffects[i]->DeactivateSystem();
					ParticalEffects[i]->DestroyComponent();
					ParticalEffects.RemoveAt(i);
				}
				i++;
			}
		}
	}
}

void ABaseCharacter::GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone)
{
	if (GetMesh())
	{
		Component = GetMesh();
		Offset = EffectOffset;
		AttachedBone = "Pelvis";
	}
}




//��������

// NET
void ABaseCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ABaseCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (GetController() && !GetController()->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.f, Yaw, 0.f)));
	}
}

void ABaseCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}
void ABaseCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharackterUpdate();
}

void ABaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABaseCharacter, MovementState);
	DOREPLIFETIME(ABaseCharacter, CurrentWeapon);
	DOREPLIFETIME(ABaseCharacter, bRightHandState);
	DOREPLIFETIME(ABaseCharacter, bMeleeWeaponState);
	DOREPLIFETIME(ABaseCharacter, CurrentWeaponIndex);
	DOREPLIFETIME(ABaseCharacter, EffectAdd);
	DOREPLIFETIME(ABaseCharacter, EffectRemove);
	DOREPLIFETIME(ABaseCharacter, Effects);
	//DOREPLIFETIME(ABaseCharacter, bIsAlive);
}

void ABaseCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ABaseCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0.f, 0.f, 0.f), FName("spine_03"));
}




bool ABaseCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i<Effects.Num(); i++)
	{
		if (Effects[i])
		{
			WroteSomething |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return WroteSomething;
}

void ABaseCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* MontageToPlay)
{
	if (GetMesh())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(MontageToPlay);
	}
	
}



