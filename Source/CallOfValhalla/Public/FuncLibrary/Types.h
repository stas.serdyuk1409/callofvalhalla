// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "../Public/COVStateEffect.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	AimState UMETA(DisplayName = "AimState"),
	WalkState UMETA(DisplayName = "WalkState"),
	RunState UMETA(DisplayName = "RunState"),
	SprintState UMETA(DisplayName = "SprintState")
};
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Bow UMETA(DisplayName = "Bow"),
	Crossbow UMETA(DisplayName = "Crossbow"),
	Cannon UMETA(DisplayName = "Cannon"),
	Throwing UMETA(DisplayName = "Throwing"),
	Melee UMETA(DisplayName = "Melee")
};
USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeed = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeed = 600.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintSpeed = 900.f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TSubclassOf<class AProjectileWeaponBase> ProjectileWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		UStaticMesh* ProjectileMesh=nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		FTransform ProjectileMeshTransform = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		UParticleSystem* ProjectileTrialFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		FTransform ProjectileTrialFXTransform = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TSubclassOf<UCOVStateEffect> Effect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fProjectileDamage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fProjectileLifeTime = 6.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fProjectileInitSpeed = 2000.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fProjectileGravityScale = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		bool bIsLikeBomb = false;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		USoundBase* ProjectileHitSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		UParticleSystem* ExploudeFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		USoundBase* ExploudSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fProjectileMaxRadiusDamage = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fProjectileMinRadiusDamage = 400.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fExploudMaxDamage = 40.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fCoefDecreaseDamage = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float fTimeToExploud = 1.f;


	////hit fx
};
USTRUCT(BlueprintType)
struct FAddicionalWeaponInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
		int32 Round = 10;
};
// ��������� ������������� �������� ������
USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fAim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fAim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fAim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fAim_StateDispersionAimReduction = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fWalk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fWalk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fWalk_StateDispersionAimReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fRun_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fRun_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fRun_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float fRun_StateDispersionAimReduction = 0.1f;

};

USTRUCT(BlueprintType)
struct FDropMesh
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fDropTime = -1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fDropLifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FVector DropImppulseMesh = FVector();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fPowerImpulse = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fImpulseRandomDespersion = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fCustomMass = 0.f;
};
USTRUCT(BlueprintType)
struct FDropOtherMesh
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		UStaticMesh* DropOtherMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fDropTime = -1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fDropLifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FVector DropImppulseMesh = FVector();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fPowerImpulse = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fImpulseRandomDespersion = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		float fCustomMass = 0.f;
};
USTRUCT(BlueprintType)
struct FDropWeapon
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		UStaticMesh* StaticWeaponMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		USkeletalMesh* SkeletalWeaponMesh = nullptr;

};
// ��������� � �������� � ��������, ��������� ������
USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Class")
		TSubclassOf<class AWeaponBase> WeaponClass = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float fUsingSpeedRate = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float fReUseTime = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 NumberProjectileByShoot = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float fWeaponDamage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float fCritChance = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float fPowerCrit = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float fArmorPenetration = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float fAccuracy = 0.9f;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float fTraceWeaponDamage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float fTraceDistance = 2000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FDropOtherMesh DropOtherMeshSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FDropMesh DropMeshSetting;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		UAnimMontage* AnimUsingWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		UAnimMontage* AnimReUsingWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		float fSwitchTimeToWeapon = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FName WeaponName = "Weapon";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		EWeaponType WeaponType = EWeaponType::Bow;

	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion DispersionWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileInfo ProjectileSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundUseWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundReUseWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectUseWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* OtherUsingWeaponFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
		UDecalComponent* DecalOnHit = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Add")
		bool RightHand = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Add")
		bool MeleeWeapon = true;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FName NameItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FAddicionalWeaponInfo AddicionalWeaponInfo;
};
USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		EWeaponType WeaponType = EWeaponType::Bow;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 Cout = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 MaxCout = 100;
};
USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		UStaticMesh* StaticMeshDropItem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		USkeletalMesh* SkeletalMeshDropItem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		FWeaponSlot WeaponSlotInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		FTransform ActorTransform = FTransform();
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		EWeaponType TypeWeaponInfo;
};

UCLASS()
class CALLOFVALHALLA_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UCOVStateEffect> AddEffectClass, EPhysicalSurface SurfaceType);
	UFUNCTION(BlueprintCallable)
	static void ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* Target, FVector Offset, FName Socket);

};
