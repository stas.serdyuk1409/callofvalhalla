// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "COVCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class CALLOFVALHALLA_API UCOVCameraShake : public UMatineeCameraShake
{
	GENERATED_BODY()
public:
	UCOVCameraShake();
};
