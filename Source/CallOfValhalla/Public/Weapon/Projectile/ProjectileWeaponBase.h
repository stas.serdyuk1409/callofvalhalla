// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/FuncLibrary/Types.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "ProjectileWeaponBase.generated.h"


UCLASS()
class CALLOFVALHALLA_API AProjectileWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectileWeaponBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereCollision = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UParticleSystemComponent* ProjectileFX = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UProjectileMovementComponent* ProjectileMovementComponent;
	UPROPERTY(BlueprintReadOnly)
	FProjectileInfo ProjectileSetting;

	
	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	void InitProjectile(FProjectileInfo InitParam);
	UFUNCTION()
	virtual void ProjectileCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
	void ProjectileCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void ProjectileCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
	virtual void ImpactProjectile();

	//Net
	UFUNCTION(NetMulticast, Reliable)
		void InitVisualMeshProjectile_Multicast(UStaticMesh* NewMesh, FTransform NewTransform);
	UFUNCTION(NetMulticast, Reliable)
		void InitVisualTrailProjectile_Multicast(UParticleSystem* NewEmmitter, FTransform NewTransform);

	UFUNCTION(NetMulticast, Reliable)
		void SpawnHitDecal_Multicast(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult Hit);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnHitFX_Multicast(UParticleSystem* FX, FHitResult Hit);
	UFUNCTION(NetMulticast, Reliable)
		void SpawnHitSound_Multicast(USoundBase* Sound, FHitResult Hit);
};
