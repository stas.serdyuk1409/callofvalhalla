// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "COVGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class CALLOFVALHALLA_API ACOVGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
};
